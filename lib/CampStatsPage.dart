import 'dart:math';

import 'package:flutter/material.dart';
import 'package:VB/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:VB/camplist/CampList.dart';
import 'package:VB/main.dart';
import 'package:badges/badges.dart';


class CampStatsPage extends StatefulWidget {
  CampStatsData data;
  CampStatsPage({this.data});
  @override
  _CampStatsState createState() => new _CampStatsState(NewData: data);
}

class _CampStatsState extends State<CampStatsPage> {
  CampStatsData NewData;
  _CampStatsState({this.NewData});
  SharedPreferences sharedPreferences;
  double cost;
  int completed;
  double CT=0.0;

  _moveToSignInScreen()
  {
    final data = Data(pUrl: NewData.pUrl, ApiKey: NewData.ApiKey);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MainPage(data: data)),
    );
  }

  @override
  Widget build(BuildContext context) {
    //-//print(NewData.id);
    cost=NewData.cost;
    completed=NewData.completed;
    if(cost==null)
      {
        cost=0;
      }
    if(completed==null)
    {
      completed=0;
    }
    if(completed>=cost && completed>0)
      {
        CT=cost/completed;
      }else
        {
          CT=0;
        }

    return
      WillPopScope(
          onWillPop: () {
            _moveToSignInScreen();
          },
     child: Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0), // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                final data = Data(pUrl: NewData.pUrl, ApiKey: NewData.ApiKey);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage(data: data)),
                );
              },
            ),
            title:
            Image.asset('images/logo.png', fit: BoxFit.cover, height: 32.0),
            centerTitle: true,
            backgroundColor: Color(0XFF000000),
            iconTheme: new IconThemeData(color: Color(0XFF97999B)),
          )),
      body:
      Row(
        children: <Widget>[
      Expanded(child:GridView.count(
        crossAxisCount: 1,
        children: List.generate(1, (index) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
                children: <Widget>[
            Card(
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            shadowColor: Color(0X1A000000),
              elevation: 15.0,
            child: Container(
              child: Column(
                children: <Widget>[
                  Align(
                  alignment: Alignment.topRight,
                      child: (
                          Badge(
                            toAnimate: false,
                            shape: BadgeShape.square,
                            badgeColor: Color(0XFFFF4444),
                            borderRadius: BorderRadius.circular(4),
                            badgeContent: Icon(Icons.attach_money, color: Color(0XFFFFFFFF), size: 16.0)
                          )
                      )
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                    margin: EdgeInsets.only(bottom: 6.0),
                    child: Column(
                      children: [
                        Text(cost.toString(),
                            style: TextStyle(
                              color: Color(0xFF000000),
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w600,
                              fontSize: 22.0,
                            )),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                    color: Color(0XFF000000),
                    child: Column(
                      children: [
                        Text("Cost",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w300,
                              fontSize: 14.0,
                            )),
                      ],
                    ),
                  ),
                  // Container(
                  //   width: MediaQuery.of(context).size.width,
                  //   padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                  //   color: Color(0xFFABB2B9),
                  //   child: Column(
                  //     children: [
                  //       Text(NewData.id.toString(),
                  //           style: TextStyle(
                  //             color: Colors.black,
                  //             fontFamily: "Poppins",
                  //             fontWeight: FontWeight.w300,
                  //             fontSize: 15.0,
                  //           )),
                  //     ],
                  //   ),
                  // )
                ],
              ),
            ),
          )
          ],
          ),
          );
        }),
      ),


    ),
          Expanded(child:GridView.count(
            crossAxisCount: 1,
            children: List.generate(1, (index) {
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Card(
                      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                      shadowColor: Color(0X1A000000),
                      elevation: 15.0,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Align(
                                alignment: Alignment.topRight,
                                child: (
                                    Badge(
                                        toAnimate: false,
                                        shape: BadgeShape.square,
                                        badgeColor: Color(0xFF2ECC71),
                                        borderRadius: BorderRadius.circular(4),
                                        badgeContent: Icon(Icons.check, color: Color(0XFFFFFFFF), size: 16.0)
                                    )
                                )
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                              margin: EdgeInsets.only(bottom: 6.0),
                              child: Column(
                                children: [
                                  Text(completed.toString(),
                                      style: TextStyle(
                                        color: Color(0xFF000000),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22.0,
                                      )),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                              color: Color(0XFF000000),
                              child: Column(
                                children: [
                                  Text("Completed",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w300,
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                            ),
                            // Container(
                            //   width: MediaQuery.of(context).size.width,
                            //   padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                            //   color: Color(0xFFABB2B9),
                            //   child: Column(
                            //     children: [
                            //       Text(NewData.id.toString(),
                            //           style: TextStyle(
                            //             color: Colors.black,
                            //             fontFamily: "Poppins",
                            //             fontWeight: FontWeight.w300,
                            //             fontSize: 15.0,
                            //           )),
                            //     ],
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              );
            }),
          ),


          ),
          Expanded(child:GridView.count(
            crossAxisCount: 1,
            children: List.generate(1, (index) {
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Card(
                      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                      shadowColor: Color(0X1A000000),
                      elevation: 15.0,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Align(
                                alignment: Alignment.topRight,
                                child: (
                                    Badge(
                                        toAnimate: false,
                                        shape: BadgeShape.square,
                                        badgeColor: Color(0XFFFBE039),
                                        borderRadius: BorderRadius.circular(4),
                                        badgeContent: Icon(Icons.local_atm, color: Color(0XFF000000), size: 16.0)
                                    )
                                )
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                              margin: EdgeInsets.only(bottom: 6.0),
                              child: Column(
                                children: [
                                  Text(CT.toString(),
                                      style: TextStyle(
                                        color: Color(0xFF000000),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22.0,
                                      )),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                              color: Color(0XFF000000),
                              child: Column(
                                children: [
                                  Text("Cost/Transfer",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w300,
                                        fontSize: 13.0,
                                      )),
                                ],
                              ),
                            ),
                            // Container(
                            //   width: MediaQuery.of(context).size.width,
                            //   padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
                            //   color: Color(0xFFABB2B9),
                            //   child: Column(
                            //     children: [
                            //       Text(NewData.id.toString(),
                            //           style: TextStyle(
                            //             color: Colors.black,
                            //             fontFamily: "Poppins",
                            //             fontWeight: FontWeight.w300,
                            //             fontSize: 15.0,
                            //           )),
                            //     ],
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              );
            }),
          ),


          ),
        ],
      )));
  }

}
