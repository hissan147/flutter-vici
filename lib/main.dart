import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:VB/camplist/CampList.dart';
import 'package:VB/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
      theme: ThemeData(accentColor: Colors.white70),
    );
  }
}

class MainPage extends StatefulWidget {
  Data data;
  MainPage({this.data});

  @override
  _MainPageState createState() => new _MainPageState(NewData: data);
}
class MySharedPreferences {
  MySharedPreferences._privateConstructor();

  static final MySharedPreferences instance =
  MySharedPreferences._privateConstructor();

  Future<String> getBooleanValue(String key) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    return myPrefs.getString(key) ?? "";
  }
}

class _MainPageState extends State<MainPage> {
  Data NewData;
  String url,token;
  _MainPageState({this.NewData});


  double CBalance = 0;
  SharedPreferences sharedPreferences;
  @override
  void initState() {
    super.initState();

    checkLoginStatus();

    MySharedPreferences.instance
        .getBooleanValue("url")
        .then((value) => setState(() {
      url= value;
    }));

    if(NewData != null){
      GetBalance(url);
    }
    GetBalance(url);
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));
    return
      WillPopScope(
        onWillPop: () {
          _onBackPressed();
    },
    child:      Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0), // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                _onBackPressed();
              },
            ),
            title:
                Image.asset('images/logo.png', fit: BoxFit.cover, height: 32.0),
            centerTitle: true,
            backgroundColor: Color(0XFF000000),
            iconTheme: new IconThemeData(color: Color(0XFF97999B)),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  _onBackPressed();
                },
                child: Icon(Icons.power_settings_new, color: Color(0XFF97999B)),
              ),
            ],
          )),
      body: CampList(pData: NewData),
      // bottomSheet: Container(
      //   height: kToolbarHeight,
      //   child: AppBar(
      //     leading: Icon(Icons.account_balance_wallet_rounded,
      //         color: Color(0XFF000000), size: 26.0),
      //     title: Align(
      //       child: new Text(CBalance.toString(), //// BALANCE HERE
      //           style: TextStyle(
      //             color: Color(0XFF333333),
      //             fontSize: 16.0,
      //             fontFamily: "Poppins",
      //             fontWeight: FontWeight.w600,
      //           )),
      //       alignment: Alignment(-1.15, 0),
      //     ),
      //     backgroundColor: Color(0XFFFBE039),
      //   ),
      // ),
    ));
  }

  // HELPING FUNCTIONS
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("api_token") == null) {
      String ss=sharedPreferences.getString("api_token");

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }
  }
  GetBalance(GBurl) async {
    var GBl = GBurl.split(".dialer360");
    var SwitchUrl = "switch";
    var UrlPart = GBl[0];
    var AccountName = UrlPart.split("vb");
    if (UrlPart == "vbcoverage") {
      SwitchUrl = "switch2";
    }
    String FinalGBl = "http://" + SwitchUrl + ".dialer360.com:88/index.php/customer_balance?account=" + AccountName[1];
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["account"] == "inactive") {
        return _Ftoast("Account in Inactive to show your Balance, Please contact with Administration", 20, Colors.black, Colors.white);
      }
      if (JBData["balance"] == null) {
        setState(() { CBalance = 0; });
      } else {
            setState(() { CBalance = double.parse((JBData["balance"]).toStringAsFixed(2)); });
      }
    } else {
      print(response.body);
    }
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Container(
          padding: EdgeInsets.only(top: 20.0, bottom: 5.0),
          child: Text('ATTENSION!',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
              )),
        ),
        content: Container(
          padding: EdgeInsets.only(top: 0.0, bottom: 15.0),
          child: Text('Are you sure you want to logout?',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w300,
                fontSize: 12.0,
              )),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text("NO",
                style: TextStyle(
                  color: Color(0XFFFF4444),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
          new FlatButton(
            onPressed: () {
              sharedPreferences.clear();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
            },
            child: Text("YES",
                style: TextStyle(
                  color: Color(0XFF000000),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
        ],
      ),
    ) ??
        false;
  }
  _Ftoast(MsG, SeC, bgClr, TxtClr) {
    Fluttertoast.showToast(
      msg: MsG,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.SNACKBAR,
      timeInSecForIosWeb: SeC,
      backgroundColor: bgClr,
      textColor: TxtClr,
    );
  }
}
